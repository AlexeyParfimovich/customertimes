@isTest
@TestVisible
private class TestDataFactory {
    
    public static void insertAccountContactTestData(Integer numRecords) {
        
        AccountContact__c[] wkps = new List<AccountContact__c>();
        Account[] accts = new List<Account>();
        Contact[] cons = new List<Contact>();

        // Create Accounts
        for (Integer i = 0; i < numRecords; i+=1) {
            accts.add(new Account(Name='TestAccount' + i));
        }
        insert accts;

        // Create Contacts associated with Accounts
        for (Integer i = 0; i < numRecords; i+=1) {
            cons.add(new Contact(
                FirstName='Test',
                LastName='Contact' + i,
                AccountId=accts[i].ID));
        }
        insert cons;

        // // Create of workplaces for contacts, started from second contact record
        // for (Integer i = 1; i < numRecords; i+=1 ) {
        //     for (Integer j = 0; j < i; j+=1){
        //         wkps.add(new AccountContact__c(
        //             Contact__c = cons[i].Id,
        //             Account__c = accts[j].Id,
        //             Name='TestWorkplace' + j));
        //     }
        // }
        // insert wkps;
    }

    public static List<AccountContact__c> createContactWorkplace(Integer numWorkplaces) {
        
        AccountContact__c[] wkps = new List<AccountContact__c>();

        Contact con = [SELECT Id FROM Contact 
                        WHERE LastName='Contact0'
                        LIMIT 1];
        Account[] accts =  [SELECT Id FROM Account 
                             LIMIT :numWorkplaces];

        for (Integer i = 0; i < numWorkplaces; i+=1) {
            wkps.add(new AccountContact__c(
                Name='TestWorkplace' + i,
                Account__c = accts[i].Id,
                Contact__c = con.Id));      
        }               
        
        return wkps;
    }

}