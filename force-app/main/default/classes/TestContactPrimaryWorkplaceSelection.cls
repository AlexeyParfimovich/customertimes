@isTest
private with sharing class TestContactPrimaryWorkplaceSelection {

    // Create test data
    @TestSetup
    static void makeData() {
        TestDataFactory.insertAccountContactTestData(3);
    }

    // Single action
    @isTest static void testInsertFirstWorkplace() {
        
        AccountContact__c wkp = TestDataFactory.createContactWorkplace(1)[0];
        
        Test.startTest();
        
        Database.SaveResult result = Database.insert(wkp);
        
        Test.stopTest();

        System.assert(result.isSuccess(),'Contact`s workplace inserting fails');

        wkp = [SELECT isPrimary__c FROM AccountContact__c 
                WHERE Id = :result.getId()];

        System.assertEquals(true, wkp.isPrimary__c, 'Setting isPrimary fails for the first workplace');
    }

    @isTest static void testInsertSecondWorkplace() {
        
        AccountContact__c[] wkps = TestDataFactory.createContactWorkplace(2);
        insert wkps[0];
        
        Test.startTest();

        Database.SaveResult result = Database.insert(wkps[1]);
        
        Test.stopTest();

        System.assert(result.isSuccess(),'Contact`s workplace inserting fails');

        AccountContact__c wkp = [SELECT isPrimary__c FROM AccountContact__c
                                  WHERE Id = :result.getId()];

        System.assertEquals(false, wkp.isPrimary__c, 'isPrimary is set incorrectly, the primary workplace has already been assigned');

    }

    @isTest static void testSetPrimaryWorkplace() {

        AccountContact__c[] wkps = TestDataFactory.createContactWorkplace(3);
        Id conId = wkps[0].Contact__c;

        insert wkps;

        Id primaryWkpId = [SELECT Id FROM AccountContact__c
                            WHERE Contact__c = :conId AND isPrimary__c = true LIMIT 1].Id;

        AccountContact__c wkp = [SELECT isPrimary__c FROM AccountContact__c
                                  WHERE Contact__c = :conId AND isPrimary__c = false LIMIT 1];

        wkp.isPrimary__c = true;                         

        Test.startTest();

        Database.SaveResult result = Database.update(wkp);
        
        Test.stopTest();

        System.assert(result.isSuccess(),'Contact`s workplace updating fails');

        wkp = [SELECT isPrimary__c FROM AccountContact__c
                WHERE Id = :primaryWkpId];

        System.assertEquals(false, wkp.isPrimary__c, 'isPrimary is not unchecked, after the primary workplace was changed');

    }

    @isTest static void testUnsetPrimaryWorkplace() {

        AccountContact__c[] wkps = TestDataFactory.createContactWorkplace(3);
        Id conId = wkps[0].Contact__c;

        insert wkps;

        AccountContact__c wkp = [SELECT isPrimary__c FROM AccountContact__c
                                  WHERE Contact__c = :conId AND isPrimary__c = true LIMIT 1];
        Id primaryWkpId = wkp.Id;                          

        wkp.isPrimary__c = false;                         

        Test.startTest();

        Database.SaveResult result = Database.update(wkp);
        
        Test.stopTest();

        System.assert(result.isSuccess(),'Contact`s workplace updating fails');

        Integer cnt = [SELECT COUNT() FROM AccountContact__c
                        WHERE Id != :primaryWkpId AND Contact__c = :conId AND isPrimary__c = true LIMIT 1];

        System.assertEquals(1, cnt, 'isPrimary is not checked, after the primary workplace was changed');
 
    }

    @isTest static void testDeletePrimaryWorkplace() {

        AccountContact__c[] wkps = TestDataFactory.createContactWorkplace(3);
        Id conId = wkps[0].Contact__c;

        insert wkps;

        AccountContact__c wkp = [SELECT isPrimary__c FROM AccountContact__c
                                  WHERE Contact__c = :conId AND isPrimary__c = true LIMIT 1];                                          

        Test.startTest();

        Database.DeleteResult result = Database.delete(wkp);
        
        Test.stopTest();

        System.assert(result.isSuccess(),'Contact`s workplace deleting fails');

        Integer cnt = [SELECT COUNT() FROM AccountContact__c
                        WHERE Contact__c = :conId AND isPrimary__c = true LIMIT 1];

        System.assertEquals(1, cnt, 'isPrimary is not checked, after the primary workplace was deleted');
    }
}