public with sharing class Logger {

    private static String getBatchClassName(Id jobId) {
        return [SELECT ApexClass.Name FROM AsyncApexJob WHERE Id = :jobId LIMIT 1][0].ApexClass.Name;
    }

    private static BatchLog__e fillLogFromContext(Database.BatchableContext context) {

        return new BatchLog__e(
            User_Name__c = UserInfo.getName(),
            Context_Id__c = context.getJobId(),
            Context_Name__c = 'batch:' + getBatchClassName(context.getJobId())
        );
    }

    public static void log(Database.BatchableContext context, Exception ex, LoggingLevel level) {

        BatchLog__e logEvent = fillLogFromContext(context);

        logEvent.Logging_Level__c = level.name();
        logEvent.Stack_Trace_String__c = ex.getStackTraceString();
        logEvent.Message__c = ex.getMessage();

        Eventbus.publish(logEvent);

        System.debug(logEvent);
    }

    public static void log(Database.BatchableContext context, String message, LoggingLevel level) {

        BatchLog__e logEvent = fillLogFromContext(context);

        logEvent.Logging_Level__c = level.name();
        logEvent.Message__c = message;

        Eventbus.publish(logEvent);
    } 
}