public with sharing class AccountContactTriggerHandler {

    //@future(callout=true)
    private static void updateWorkplaces(Set<AccountContact__c> updateSet) {

        updateSet.remove(null);

        if (updateSet.size() > 0) {
            update new List<AccountContact__c>(updateSet);
       }
    }

    // Get a set of unique pairs account+contact from the list
    private static Set<Id[]> getUniqueRecordSet(AccountContact__c[] recordList) {
        Set<Id[]> uniqueRecords = new Set<Id[]>();
        for(AccountContact__c item: recordList) {
            uniqueRecords.add(new List<Id>{item.Account__c, item.Contact__c});
        }
        return uniqueRecords;
    }

    // Get a set of unique contacts from the list
    private static Set<Id> getUniqueContactSet(AccountContact__c[] recordList) {
        Set<Id> uniqueContacts = new Set<Id>();
        for(AccountContact__c item: recordList) {
            uniqueContacts.add(item.Contact__c);
        }
        return uniqueContacts;
    }

    // Process before insert
    public static void beforeInsert(List<AccountContact__c> newList) {

        // Get a set of unique pairs account+contact from the list
        Set<List<Id>> uniqueRecords = getUniqueRecordSet(newList);
        
        // Get a set of unique contacts from the list
        List<id> uniqueContacts = new List<id>(getUniqueContactSet(newList));
               
        // Get all records for specific contacts
        ContactWorkplaces workplaces = new ContactWorkplaces(uniqueContacts);
        
        // Insert list processing
        for (AccountContact__c newItem: newList) {

            Boolean isPrimary = true;

            // Check if there are duplicated workplaces in the newList
            List<Id> record = new List<Id>{newItem.Account__c, newItem.Contact__c};
            if (!uniqueRecords.contains(record)) {
                newItem.addError('A workplace has already been created for the account ' + newItem.Account__c);
            }
            uniqueRecords.remove(record);

            // Checking existing workplaces in the Org
            for (AccountContact__c wkp: workplaces.getWorkplaces()) {
                // If there are duplicated workplaces in the Org
                if (wkp.Account__c == newItem.Account__c && wkp.Contact__c == newItem.Contact__c) {
                    newItem.addError('A workplace has already been created for the account ' + newItem.Account__c);
                }

                // If there are primary workplaces in the Org
                if (wkp.Contact__c == newItem.Contact__c && wkp.isPrimary__c == true) {
                    isPrimary = false;
                    break;
                }
            }

            for (AccountContact__c prevItem: newList){
                if (prevItem.Contact__c == newItem.Contact__c && prevItem.IsPrimary__c == true) {
                    isPrimary = false;
                    break;
                }
            }

            newItem.isPrimary__c = isPrimary;
        }
    }

    // Process after update
    public static void afterUpdate(Map<Id, AccountContact__c> oldMap, Map<Id, AccountContact__c> newMap) {
        
        // Set of records to update
        Set<AccountContact__c> updateSet = new Set<AccountContact__c>();

        // Get a set of unique contacts from the newlist
        List<id> uniqueContacts = new List<id>(getUniqueContactSet(newMap.values()));
                    
        // Get all records for specific contacts
        ContactWorkplaces workplaces = new ContactWorkplaces(uniqueContacts);

        for (Id key : newMap.keySet()) {
            AccountContact__c oldItem = oldMap.get(key);
            AccountContact__c newItem = newMap.get(key);
        
            // If IsPrimary was changed from false to true, IsPrimary should be unchecked from last primary workplace (based on CreatedDate field)
            if (oldItem.isPrimary__c == false && newItem.isPrimary__c == true) {
                updateSet.add(workplaces.changePrimaryWorkplace(false, newItem.Id, newItem.Contact__c));
            } 

            // If IsPrimary was changed from true to false, IsPrimary should be checked on the last workplace (based on CreatedDate field)
            else if (oldItem.isPrimary__c == true && newItem.isPrimary__c == false) {
                updateSet.add(workplaces.changePrimaryWorkplace(true, newItem.Id, newItem.Contact__c));
            }
        }

        updateWorkplaces(updateSet);
    }

    // Process after delete
    public static void afterDelete(List<AccountContact__c> oldList) {

        // Set of records to update
        Set<AccountContact__c> updateSet = new Set<AccountContact__c>();

        // Get a set of unique contacts from the newlist
        List<id> uniqueContacts = new List<id>(getUniqueContactSet(oldList));
                    
        // Get all records for specific contacts
        ContactWorkplaces workplaces = new ContactWorkplaces(uniqueContacts);

        for (AccountContact__c item: oldList) {
            if (item.isPrimary__c) {
                // If primary workplace was deleted, IsPrimary should be checked on the last workplace (based on CreatedDate field)
                updateSet.add(workplaces.changePrimaryWorkplace(true, item.Id, item.Contact__c));
            }
        }

        updateWorkplaces(updateSet);
    }
}