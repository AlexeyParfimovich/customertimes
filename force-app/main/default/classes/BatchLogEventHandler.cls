public with sharing class BatchLogEventHandler {
    public static void handle(List<BatchLog__e> events) {

        List<Log__c> logs = new List<Log__c>();

        for (BatchLog__e event: events) {
            logs.add(new Log__c(
                User_Name__c = event.User_Name__c,
                Context_Id__c = event.Context_Id__c,
                Context_Name__c = event.Context_Name__c,
                Logging_Level__c = event.Logging_Level__c,
                Stack_Trace_String__c = event.Stack_Trace_String__c,
                Message__c = event.Message__c
            ));
        }

        if (logs.size() > 0) {
            insert logs;
        }
    }
}