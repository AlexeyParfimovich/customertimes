public with sharing class ContactWorkplaces {

    private List<AccountContact__c> workplaces; 

    public ContactWorkplaces(Id[] contacts) {
        workplaces = [
            SELECT Account__c, Contact__c, isPrimary__c FROM AccountContact__c
            WHERE Contact__c IN :contacts
            ORDER BY Contact__c, CreatedDate DESC
        ];
    }

    public List<AccountContact__c> getWorkplaces() {
        return workplaces;
    }

    public AccountContact__c getLatestWorkplace(Id workplaceId, Id contactId, Boolean isPrimary) {
        for (AccountContact__c workplace: workplaces) {
            if (workplace.Contact__c == contactId && workplace.isPrimary__c == isPrimary && workplace.Id != workplaceId) {
                return workplace;
            }
        }
        return null;
    }

    public AccountContact__c changePrimaryWorkplace(Boolean isPrimary, Id workplaceId, Id contactId) {

        AccountContact__c primaryWorkplace = getLatestWorkplace(workplaceId, contactId, true);

        if (!isPrimary && primaryWorkplace != null) {
            primaryWorkplace.isPrimary__c = false;
            return primaryWorkplace;
        } 

        if (isPrimary && primaryWorkplace == null) {    
            AccountContact__c latestWorkplace = getLatestWorkplace(workplaceId, contactId, false);

            if (latestWorkplace != null) {
                latestWorkplace.isPrimary__c = true;
                return latestWorkplace;
            }
        } 

        return null;
    }

}