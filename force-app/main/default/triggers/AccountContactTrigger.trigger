trigger AccountContactTrigger on AccountContact__c (before insert, after update, after delete) {
    if (Trigger.isBefore) {
        if (Trigger.isInsert) {
            // Process before insert
            AccountContactTriggerHandler.beforeInsert(Trigger.new);
        }
    } else if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            // Process after update
            AccountContactTriggerHandler.afterUpdate(Trigger.oldMap, Trigger.newMap);
        } else if (Trigger.isDelete) {
            // Process after detele
            AccountContactTriggerHandler.afterDelete(Trigger.old);
        }        
    }
}