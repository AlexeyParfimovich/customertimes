trigger BatchLogEventTrigger on BatchLog__e (after insert) {
  BatchLogEventHandler.handle(Trigger.new);
}