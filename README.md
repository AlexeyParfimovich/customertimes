## HOMEWORK 06

### Building Platform Events Logging Frameworks  

Using Batch example from the zip file implement logging framework which will be built using platform events and additional Log__c object.  

Logging framework should have following API:
- Logger.log(Database.BatchableContext context, Exception ex, LoggingLevel level);
- Logger.log(Database.BatchableContext context, String message, LoggingLevel level).  

### Task Description  

If an error occurs in the batch execution, the exception should be logged by Logger framework (LF).  
LF should utilize platform events under the hood for error logs.  
An original exception should be still thrown from the batch after event publishing.  

The logging level is ERROR.  

Context_Name__c =  "batch: \<name of the batch\>."  

If batch finishes successfully log__c object should be created with INFO level and message “batch finished successfully”  

#### Question to think about:
- Why can't we use synchronous context and create Log__c object directly in Logger class?  

#### Answer: 
- Maybe because the transaction is not committed, so on rollback, the Log_C object will not be saved? 

### Notes  

Platform events get executed under process automation process context.  

In order to see logs from trigger go to setup|debug log|-create trace flog for process automation user.  

Platform event trigger should have publish immediately behavior.  
 
For testing purposes you can either implicitly produce an error by specifying incorrect data type in the field, writing some validation rule etc.
Or you can fire implicitly custom exception at the end of execution flow.  
